<?php include("config.php"); ?>

<!DOCTYPE html>
<html>
<head>
    <title>Stock ATK Management System | PT Nittsulemo Indonesia Logistik</title>
</head>

<body>
    <header>
        <h3>Stock ATK yang ada</h3>
    </header>

    <nav>
        <a href="form-input.php">[+] Tambah Baru</a>
    </nav>

    <br>

    <table border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Item</th>
            <th>Stock Awal</th>
            <th>Item Masuk</th>
            <th>Item Keluar</th>
            <th>Stock</th>
            <th>Tanggal</th>
            <th>Divisi</th>
            <th>Harga</th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $sql = "SELECT * FROM list_item";
        $query = mysqli_query($db, $sql);

        while($item = mysqli_fetch_array($query)){
            echo "<tr>";

            echo "<td>".$item['no']."</td>";
            echo "<td>".$item['nama_item']."</td>";
            echo "<td>".$item['stock_awal']."</td>";
            echo "<td>".$item['item_masuk']."</td>";
            echo "<td>".$item['item_keluar']."</td>";
            echo "<td>".$item['stock']."</td>";
            echo "<td>".$item['tanggal']."</td>";
            echo "<td>".$item['divisi']."</td>";
            echo "<td>".$item['harga']."</td>";
            echo "<td>".$item['keterangan']."</td>";

            echo "<td>";
            echo "<a href='form-edit.php?id=".$item['no']."'>Edit</a> | ";
            echo "<a href='hapus.php?id=".$item['no']."'>Hapus</a>";
            echo "</td>";

            echo "</tr>";
        }
        ?>

    </tbody>
    </table>

    <p>Total: <?php echo mysqli_num_rows($query) ?></p>

    </body>
</html>
